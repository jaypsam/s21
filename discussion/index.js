console.log("Hello World");

//Array

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);

let studentID = ["2020-1923" , "2020-1924", "2020-1925", "2020-1926", "2020-1927"]
console.log(studentID);


/*
	Arrays
		- is used to store multiple related values in a single variable.
		- declared using the square bracket([]) also known as "Array literals"

	Syntax:
		let/const arrayName = [elementA, elementB. ... elementN]	
*/

let grades = [96.8, 87.7, 93.2, 94.6]
console.log(grades);
let computerBrands = ["Acer", "Asus","Lenovo","Neo", "Redfox","Gateway","Toshiba"];
console.log(computerBrands);

//not recommended
let mixedArray = [12, "Asus",null,undefined,{}];
console.log(mixedArray);

let mytask = [
	"drink HTML",
	"eat javascript",
	"inhale css",
	"bake sass"
];

console.log(mytask);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";
let city4 = "Beijing";

let cities = [city1, city2, city3, city4];
/*let citiesSample = ["Tokyo","Manila","New York","Bejing"];
console.log(cities);
console.log(typeof citiesSample);
console.log(cities = citiesSample);*/

//Array length property
/*
 	-to set or to get the items or elements in an array
*/

//Define element of an array
console.log(mytask.length);//4
console.log(cities.length);//4

let blankArray = [];
console.log(blankArray.length);

let fullName = "Fidel Ramos";
console.log(fullName.length);//11

//delete an array
mytask.length--
console.log(mytask.length);
console.log(mytask);

//add an array
let theBeatles = ["John","Paul","George","Ringo"];
console.log(theBeatles.length);
theBeatles.length++
console.log(theBeatles.length);
console.log(theBeatles);

//starts on 0
theBeatles[4] = "Yoby";
console.log(theBeatles);

/*
	Accessing elements of an array

	Syntax:
		arrayName[index]


*/

//if your going to recall an array
console.log(grades[0]);
console.log(computerBrands[3]);

let lakersLegend = ["Kobe","Shaq","LeBron","Magic","Kareem"];

console.log(lakersLegend[0]);
console.log(lakersLegend[2]);

//change variable create new declaration
let currentLaker = lakersLegend[2];
console.log(currentLaker);

//update variable/array
console.log("Array before the reassignment");
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol"
console.log("Array after the reassignment");
console.log(lakersLegend);

//accesing the last array if minus type --
let bullLegend = ["Jordan","Pippen","Rodman","Rose","Kukoc"];

let lastElementIndex = bullLegend.length- 1;
console.log(bullLegend.length);
console.log(bullLegend[lastElementIndex]);

//Adding items into an array in blank array

let newArray = [];
console.log(newArray);
console.log(newArray[0])
newArray[0] = "Jenny";
console.log(newArray);
newArray[1] = "Jisso";
console.log(newArray);

//Add in the end of array
newArray[newArray.length++] = "Lissa";
console.log(newArray);

// Mini activity:

/*

Part 1: Adding a value at the end of the array

	-Create a function which is able to receive a single argument and add the input at the end of the superheroes array
	-Invoke and add an argument to be passed to the function
	-Log the superheroes array in the console

	let superHeroes = ['Iron Man', 'Spiderman', 'Captain America']*/
	
/*
let superHeroes = ['Iron Man', 'Spiderman', 'Captain America'];
console.log(superHeroes.length);
superHeroes.length++
console.log(superHeroes.length);
console.log(superHeroes);


superHeroes[3] = "Gagamboy";
console.log(superHeroes);
*/
//Solution
let superHeroes = ['Iron Man', 'Spiderman', 'Captain America'];

function marvelHeroes(heroName){
	superHeroes[superHeroes.length++] = heroName;

}

marvelHeroes("Thor");
console.log(superHeroes);
/*marvelHeroes("Wanda");
console.log(superHeroes);*/

/*
Part 2: Retrieving an element using a function
	
	-Create a function which is able to receive an index number as a single argument
	-Return the element/item accessed by the index.
	-Create a global variable named heroFound and store/pass the value returned by the function.
	-Log the heroFound variable in the console.

*/

//Solution

//index of array
 function getHeroByIndex(index){
 	return superHeroes[index];
 }

 let heroFound = getHeroByIndex(2);
 console.log(heroFound);

 for(let index = 0; index < newArray.length; index++){
 	console.log(newArray[index]);
 }

 let numArray = [5,12,26,20,42,50,67,85]

 for (let index = 0; index < numArray.length; index++){

 	if (numArray[index] % 5===0){
 		console.log(numArray[index] + " is divisible by 5");
 	}else{
 		console.log(numArray[index] + " is not divisible by 5");
 	}
 }

 //Multidimensional Arrays
 /*
	-array within an array
 */

console.log(" ");
 let chestBoard = [
 	["a1, 'b1","c1","e1","f1","g1","h1"],
 	["a2, 'b2","c2","e2","f2","g2","h2"],
 	["a3, 'b3","c3","e3","f3","g3","h3"],
 	["a4, 'b4","c4","e4","f4","g4","h4"],
 	["a5, 'b5","c5","e5","f5","g5","h5"],
 	["a6, 'b6","c6","e6","f6","g6","h6"],
 	["a7, 'b7","c7","e7","f7","g7","h7"],
 	["a8, 'b8","c8","e8","f8","g8","h8"]
 ]

 console.log(chestBoard);
 console.log(chestBoard[1][4]);
 console.log("Pawn moves to: " + chestBoard[7][4]);
















































